const config = {
  clearColor: [0.0, 0.0, 0.0, 1.0],
  depthValue: 1.0,
  webglCanvasID: 'webgl',
  textCanvasID: 'textCanvas',
};

/**
 * Setup WebGL for further use.
 * @param {WebGL2RenderingContext} webgl - WebGL context
 */
function setupWebGL(webgl) {
  // Check if webgl is supported
  if (webgl === null) {
    alert('webgl not supported');
    return;
  }

  // Set buffer clear values
  webgl.clearColor(...config.clearColor);
  webgl.clearDepth(config.depthValue);

  // Enable capabilities and set depth function
  webgl.enable(webgl.DEPTH_TEST);
  webgl.depthFunc(webgl.LEQUAL);
}

/**
 * Entry point for the program.
 */
function main() {
  // Get context
  const webgl =
      document.getElementById(config.webglCanvasID).getContext('webgl2');
  const textCanvas =
      document.getElementById(config.textCanvasID).getContext('2d');

  setupWebGL(webgl);
}

document.onload(main);
