const util = {
  /**
     * Read data from file
     * @param {String} file - target file to read from
     * @return {Promise} - a promise that resolves to a `USVString`
     *                     (the file's text)
     */
  readTextFile(file) {
    return fetch(file).then((response) => {
      return response.text();
    });
  },

  /**
     * Load texture from File
     * @param {WebGL2RenderingContext} webgl - WebGL context
     * @param {String} url - Texture position
     * @return {WebGLTexture|null} - Texture
     */
  loadTexture(webgl, url) {
    // The texture
    const texture = webgl.createTexture();

    // Use texture from this point on
    webgl.bindTexture(webgl.TEXTURE_2D, texture);

    // Create temporary texture to make the texture usable before
    // the texture is downloaded
    const level = 0; // 0 is the base image level and level n is the nth mipmap reduction level
    const internalFormat = webgl.RGBA; // Specifies the color components
    const width = 1; // Specifies texture width
    const height = 1; // Specifies texture height
    const border = 0; // Specifies border width. Must be 0!
    const format = webgl.RGBA; // Specifies texel data format
    const type = webgl.UNSIGNED_BYTE; // Specifies texel data type
    const pixel = new Uint8Array([255, 111, 255, 255]); // pink

    // Specifies the two-dimensional pink texture image
    webgl.texImage2D(webgl.TEXTURE_2D, level, internalFormat, width,
        height, border, format, type, pixel);

    const image = new Image();
    image.onload = function() {
      // Use texture from this point on
      webgl.bindTexture(webgl.TEXTURE_2D, texture);

      // Specifies the two-dimensional texture image
      webgl.texImage2D(webgl.TEXTURE_2D,
          level,
          internalFormat,
          format,
          type,
          image
      );

      // Check if image is a power of 2 in both dimensions
      if ((image.width & (image.width - 1)) === 0 &&
          (image.height & (image.height - 1)) === 0) {
        webgl.generateMipmap(webgl.TEXTURE_2D);
      } else {

        webgl.texParameteri(
            webgl.TEXTURE_2D,
            webgl.TEXTURE_WRAP_S,
            webgl.CLAMP_TO_EDGE
        );
        webgl.texParameteri(
            webgl.TEXTURE_2D,
            webgl.TEXTURE_WRAP_T,
            webgl.CLAMP_TO_EDGE
        );
        webgl.texParameteri(
            webgl.TEXTURE_2D,
            webgl.TEXTURE_MIN_FILTER,
            webgl.LINEAR
        );
      }
    };
    image.src = url;

    return texture;
  },

  /**
     * Create a texture that is a single color.
     * @param {WebGL2RenderingContext} webgl - WebGL context
     * @param {number[]} color - Array with RGBA color values
     * @return {WebGLTexture} - Single color texture
     */
  createColorTexture(webgl, color) {
    // The texture
    const texture = webgl.createTexture();

    // Use texture from this point on
    webgl.bindTexture(webgl.TEXTURE_2D, texture);

    // Create temporary texture to make the texture usable before
    // the texture is downloaded
    const level = 0; // 0 is the base image level and level n is the nth mipmap reduction level
    const internalFormat = webgl.RGBA; // Specifies the color components
    const width = 1; // Specifies texture width
    const height = 1; // Specifies texture height
    const border = 0; // Specifies border width. Must be 0!
    const format = webgl.RGBA; // Specifies texel data format
    const type = webgl.UNSIGNED_BYTE; // Specifies texel data type
    const pixel = new Uint8Array(color);

    // Specifies the two-dimensional pink texture image
    webgl.texImage2D(webgl.TEXTURE_2D, level, internalFormat, width,
        height, border, format, type, pixel);

    return texture;
  },
};
export {util};
